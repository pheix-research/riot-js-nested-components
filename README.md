# Nested components rendering with RIOT.js

The problem to be investigated: [nested components](https://riot.js.org/documentation/#nested-components) rendering — in global template would be:

* ✅ page with nested components, dynamic tab navi and [lazy content rendering](https://gitlab.com/pheix-research/riot-js-nested-components/-/blob/main/admin-panel/index_v2_2.html#L63), initial [version](https://gitlab.com/pheix-research/riot-js-nested-components/-/blob/main/admin-panel/index_v2.html#L63);
* ✅ js code [to create](https://gitlab.com/pheix-research/riot-js-nested-components/-/blob/main/admin-panel/index.html#L39) dynamic tab navi with installed extensions;
* ✅ js code [to add](https://gitlab.com/pheix-research/riot-js-nested-components/-/blob/main/admin-panel/index.html#L42) components for each extension;
* ✅ all extension components and their variables [should be available](https://gitlab.com/pheix-research/riot-js-nested-components/-/blob/main/admin-panel/index.html#L100) in initial admin panel response.

## Dependencies

https://gitlab.com/pheix-io/ethelia/-/issues/4#note_1104267529

## Plunker

https://plnkr.co/plunk/Jb2OXMIyXHbn0TU2

## License information

This is free and opensource software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
